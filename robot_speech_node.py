#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import requests.packages.urllib3
from gtts import gTTS
import os
from pygame import mixer


class text2speech:
  def __init__(self):
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("speech", String, self.callback)
    rospy.loginfo("Started Text2Speech node");
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


  def play_data(self, msg):
    file_name = msg.replace(' ', '_') + '.mp3'

    if not os.path.exists(file_name):
      tts = gTTS(text=msg, lang='es')
      rospy.loginfo(rospy.get_caller_id() + ' Speech:  %s', msg)
      tts.save(file_name)

    mixer.init()
    mixer.music.load(file_name)
    mixer.music.play()


  def callback(self, data):
    self.play_data(data.data)


if __name__ == '__main__':
    # dir_path = os.path.dirname(os.path.realpath(__file__))
    # os.chdir(dir_path)

    requests.packages.urllib3.disable_warnings()
    t = text2speech();
